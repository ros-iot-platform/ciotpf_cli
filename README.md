
CLIツール


# app nodeの雛形出力  
    ./app_generator ./testspec ./ 
    第一引数に、仕様の設定ファイル（.yaml), 第二引数に、出力を保存するディレクトリを指定する。

# app実行
    ./app_runner ./LightControlApp/LightControlApp.py
    ./app_runner ./LightControlApp/LightControlApp.py --build

# device nodeの雛形出力
    ./device_generator ./test_device_spec ./ 


## Dockerイメージの再ビルド    
ciotpf_create_app_toolとかを修正した後はイメージ作り直さないとだめ  
docker build -t ciotpf/testenv ./libs --no-cache
